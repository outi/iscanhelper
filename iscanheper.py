#!/usr/bin/python
import os
import pyinotify
import datetime
import shutil
import pynotify
import pyexiv2
import sys

wm = pyinotify.WatchManager()
mask = pyinotify.IN_CLOSE_WRITE

home = os.getenv('HOME')

class FileManager(object):

    def __init__(self):
        now = datetime.datetime.now()
        self.dest_base = '%d%.2d%.2d' % (now.year, now.month, now.day)
        self.dest_root = '%s/Skany/skany_%s' % (home, self.dest_base)
        self.dest_backup = os.path.join(self.dest_root, 'raw')
        pynotify.init("iScan Helper")
        
        self.notify = pynotify.Notification("Wykryto skan", "Lorem ipsum op")
        
        try:
            os.mkdir(self.dest_root)
            os.mkdir(self.dest_backup)
        except OSError:
            pass


    def exif(self, path):
        m = pyexiv2.ImageMetadata(path)
        m.read()
        m['Exif.Image.Model'] = sys.argv[1]
        m['Exif.Photo.UserComment'] = sys.argv[2]
        m.write()


    def get_max_num(self):
        ret = 0
        for f in os.listdir(self.dest_root):
            if f.endswith('.png'):
                number = int(f.replace(sys.argv[3], '').split('.')[0])
                if ret<number:
                    ret = number
        return str(ret+1)
        
            
    def copy(self, src):
        
        dest = self.get_max_num().rjust(4,'0') + '%s.png' % sys.argv[3]
        
        print 'copy %s %s' % (src, os.path.join(self.dest_root, dest)) 
        
        shutil.copyfile(src, os.path.join(self.dest_root, dest))
        shutil.copyfile(src, os.path.join(self.dest_backup, dest))
        
        self.exif(os.path.join(self.dest_root, dest))
        self.exif(os.path.join(self.dest_backup, dest))
       
        
        os.remove(src)
        self.notify.update("Skan skopiowany", "Nowa nazwa to %s" % dest)
        self.notify.show()



class PTmp(pyinotify.ProcessEvent):
    
    def my_init(self):
        self.fmanager = FileManager()

    def process_IN_CLOSE(self, event):
        print "event %s, bn: '%s'" % (event.path, os.path.basename(event.pathname))
        if os.path.basename(event.pathname) == 'default.png':
            self.fmanager.copy(event.pathname)


if len(sys.argv) != 4:
    print sys.argv[0] + ' CAMERA FILM FILE_SUFFIX'
    sys.exit(1)

notifier = pyinotify.Notifier(wm, PTmp())

wdd = wm.add_watch(home, mask, rec=False)

while True:
    try:
        notifier.process_events()
        if notifier.check_events():
            notifier.read_events()
    except KeyboardInterrupt:
        notifier.stop()
        break
        
